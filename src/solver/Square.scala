package solver

import collection.SortedSet

class Square {
  var candidates :SortedSet[Int] = SortedSet.apply(1 to 9:_*)
  
  def getCandidates :SortedSet[Int] = {
    return candidates
  }
    
  def setCandidates(c :SortedSet[Int] ) {
    candidates = c
  }
  override def clone :Square = {
    val c = new Square
    c.setCandidates( candidates - (-1) )
    return c
  }
  
  def setValue( v :Int ){
     candidates = SortedSet(v)
  }
  def remove( v :Int ) :Boolean = {
    val r = candidates(v)
    candidates = candidates - v 
    return r
  }
  def hasDefiniteValue :Boolean = {
      return candidates.size == 1 
  }
  def getDefiniteValue :Option[Int] = {
    if( hasDefiniteValue ){
        return Some( candidates.head )
    }
    return None
  }
  override def toString :String = {
     if( candidates.size == 1 ){
       return candidates.head.toString
     }
     else if( candidates.size == 9 ){
       return " "  
     }
     return "."
  }
}