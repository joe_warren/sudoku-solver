package solver

object SudokuSolver {
    def main( args: Array[String] ){
      val s = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"

      val g = new Grid
      g.fromString(s)
      println(g)
      g.solve(0)
      println(g)
    }
}