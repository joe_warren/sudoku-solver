package solver

class Grid(){
	var grid = Array.fill[Square](9,9){ new Square }

  def fromString(s: String){
	  import scala.util.control.Exception._
		for( x <- 0 to 8 ){
			for( y <- 0 to 8){
				val value = s( x+ 9*y )
				val ivalue = catching(classOf[NumberFormatException]) opt value.toString.toInt
				if( 1 to 9 contains ivalue.getOrElse(-1) ){
				 	grid(x)(y).setValue( ivalue.get )
				}
			}
		}
	}
	def isSolved :Boolean = {
	   return grid.forall( _.forall(_.hasDefiniteValue ) )
	}
	def getSection(x :Int, y :Int) :Array[Square] = {
	   var r = new Array[Square](8)
	   var c = 0
	   val floorx = 3*(x/3)
	   val floory = 3*(y/3)
	   for( i <- floorx  to floorx+2  ){
		   for( j <- floory to floory+2){
	       if( i != x || j != y ){
	          r(c) = grid(i)(j)
	          c = c + 1
	       }
			}
	  }
	  return r
	}
	def isValid :Boolean = {
	  
	  for( x <- 0 to 8 ){
			for( y <- 0 to 8){
	      if( grid(x)(y).hasDefiniteValue ){
          val v = grid(x)(y).getDefiniteValue.get
	        if( ((0 to 8) diff List(x)).exists(v == grid(_)(y).getDefiniteValue.getOrElse(-1))){
	          return false
	        }
	        if(((0 to 8) diff List(y)).exists(v == grid(x)(_).getDefiniteValue.getOrElse(-1))){
	          return false
	        }
	        if( getSection(x, y).exists(v == _.getDefiniteValue.getOrElse(-1))){
	          return false 
	        }
	       
	      }
			}
	  }
	  return true
	}	  
	 
	def propogate :Boolean = {
	  var changed = false
	  for( x <- 0 to 8 ){
			for( y <- 0 to 8){
	      if( grid(x)(y).hasDefiniteValue ){
          val v = grid(x)(y).getDefiniteValue.get
	        changed = changed || ((0 to 8) diff List(x)).exists(grid(_)(y).remove(v))
	        changed = changed || ((0 to 8) diff List(y)).exists(grid(x)(_).remove(v))
	        changed = changed || getSection(x, y).exists(_.remove(v))
	       
	      }
			}
	  }
	  return changed
	  }


	  def copy(bild: Array[Array[Square]]):Array[Array[Square]] = {
			  val result = Array.ofDim[Array[Square]](bild.length)
					  for(x <- 0 until bild.length ) {
						  result(x) = Array.ofDim[Square](bild(x).length)
								  for(y <- 0 until bild(x).length) {
									  result(x)(y) = bild(x)(y).clone
								  }
					  }
			  return result
	  }
	
	def solve( i:Int = 0 ) :Boolean = {
	  
      println("*"*i)
	    while(propogate != false){}
	    if( isSolved ){
	      return true
	    }
	    var oldGrid = copy(grid)
	    for(x <- 0 to 8){
	      for( y <- 0 to 8){
	        if(!grid(x)(y).hasDefiniteValue){
	          val candidates = grid(x)(y).getCandidates
	          for( c <- candidates ){
	            grid(x)(y).setValue(c)
	            if( isValid ){
	              if( solve( i+1 ) ){
	                return true
	              }
	            }
	            grid = copy(oldGrid)
	          }
	        }
	      }
	    }
	  return false
	}
	
	override def toString() :String = {
	  var s = ""
	  for( y <- 0 to 8 ){
			for( x <- 0 to 8){
			   s = s.concat( grid(x)(y).toString )
			   if( x == 2 || x == 5){
			     s = s.concat("|")
			   }
			}
			s = s.concat("\n")
			if( y == 2 || y == 5){
			  s = s.concat(Array.fill(3)("-"*3).mkString("+")+"\n")
			}
	  }
	  return s
	}
}